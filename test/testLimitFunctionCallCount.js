const limitFunctionCallCount = require("../limitFunctionCallCount");

const callLimit = limitFunctionCallCount(() => "hello", 2);
console.log(callLimit());
console.log(callLimit());
console.log(callLimit());
console.log(callLimit());
