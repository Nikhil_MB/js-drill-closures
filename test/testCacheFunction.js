const cacheFunction = require("../cacheFunction");

const callCache1 = cacheFunction((m, n) => m + n);
console.log(callCache1(1, 2, 3));
console.log(callCache1(1, 2, 3));
console.log(callCache1(4, 5, 6));

const callCache2 = cacheFunction((m) => m * m);
console.log(callCache2(1, 2, 3));
console.log(callCache2(6, 2, 3));
console.log(callCache2(1, 2, 3));
