const counterFactory = require("../counterFactory");

const callCounter = counterFactory();
console.log(callCounter.decrement());
console.log(callCounter.decrement());
console.log(callCounter.increment());
