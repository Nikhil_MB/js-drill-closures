// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned

function limitFunctionCallCount(cb, n) {
  counter = 0;
  return function () {
    if (counter < n) {
      counter += 1;
      return cb();
    } else {
      return null;
    }
  };
}

module.exports = limitFunctionCallCount;
